const {
    src,
    dest,
    watch,
    parallel
} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss')
const sourcemaps = require('gulp-sourcemaps')
const cssnano = require('cssnano');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const imagemin = require('gulp-imagemin');
const notify = require('gulp-notify');
const cache = require('gulp-cache');
const webp = require('gulp-webp');

const paths = {
    scss: 'src/scss/**/*.scss',
    js: 'src/js/**/*.js',
    imagenes: 'src/img/**/*'
}

// css es una función que se puede llamar automaticamente
function css() {
    return src(paths.scss)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        // .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write('.'))
        .pipe(dest('./build/css'));
}

function imagenes() {
    return src(paths.imagenes)
        .pipe(cache(imagemin({
            optimizationLevel: 3
        })))
        .pipe(dest('build/img'));
}

function versionWebp() {
    return src(paths.imagenes)
        .pipe(webp())
        .pipe(dest('build/img'));
}


function watchArchivos() {
    watch(paths.scss, css);
    watch(paths.imagenes, imagenes);
    watch(paths.imagenes, versionWebp);
}

exports.default = parallel(css, imagenes, versionWebp, watchArchivos);